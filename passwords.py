import itertools
import string

letters = { char:[char.upper(),char.lower()] for char in string.ascii_lowercase }

shifted_chars = {
    "1":"!",
    "2":"@",
    "3":"#",
    "4":"$",
    "5":"%",
    "6":"^",
    "7":"&",
    "8":"*",
    "9":"(",
    "0":")",
    "-":"_",
    "=":"+",
    "[":"{",
    "]":"}",
    ";":":",
    "'":"\"",
    "\\":"|",
    ",":"<",
    ".":">",
    "/":"?"
    }

hax0r = {
    "a":"@",
    "o":"0",
    }

def __merge_dict(new_map, old_map):
    """
    Method join new dictionary with existing output one.

    Arguments:
    new_map - new dictionary with letters mapping
    old_nap - already existing dictionary
    """

    for key, value in new_map.items():

        current_value = old_map.get(key, [])
        extend_function = None
        if type(value) == type([]):
            extend_function = current_value.extend
        else:
            extend_function = current_value.append
        extend_function(value)
        old_map[key] = current_value

    return old_map

def merge_dicts(*dicts):
    """
    Method merge map char dictonaries into one.
    """

    output = {}

    for dictionary in dicts:
        output = __merge_dict(dictionary, output)

    return output


def word_generator(input_word, char_map):
    """
    Method takes word and dictionary. Each char in input_word replaces with list from char_map

    input_word - password phrase
    char_map - chars map
    """
    
    output_list = []

    for char in input_word:
        dict_value = char_map.get(char.lower(), [char])
        output_list.append(dict_value)

    return output_list
    
def passwords_generator(input_word, char_map):
    """
    Method takes single word and dictionary and generates different combinations basing on char map
    """
    pass_list = word_generator(input_word, char_map)
    return ["".join(x) for x in list(itertools.product(*pass_list))]    
    

def multi_passwords_generator(char_map, *args):
    """
    Method takes char_map, and list of key words and generates dictionary with combinations for each word.

    Arguments:
    char_map
    *args - key password words

    Returns:
    combos - dictionary with differents combinations of words
    """
    combos = {}

    for x in args:
        combos[x] = passwords_generator(x, char_map)

    return combos

def passwords(char_map, *words):
    output = []
    #generate sequences for password generator
    sequences = []
    [ sequences.extend(list(itertools.permutations(words,x+1))) for x in range(0, len(words)) ]

    generated_words = multi_passwords_generator(char_map, *words)

    for seq in sequences:
        pass_for_seq = [generated_words[word] for word in seq]
        output.extend(list(itertools.product(*pass_for_seq)))

    #return output
    return ("".join(x) for x in output)

a = {"a":"A","b":"B","c":"C","d":["D",0]}
b = {"a":"@","b":"8","p":['P','9'],"i":['I','1','!','|']}

char_map = merge_dicts(letters, hax0r)

if __name__ == '__main__':
    words = input("Please type words: ")
    words = words.split(" ")
    for x in passwords(char_map, *words):
        print(x)

    
